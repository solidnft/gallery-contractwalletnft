/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['openseauserdata.com'],
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'openseauserdata.com',
        port: '',
        pathname: '/files/**',
      },
    ],
  },
}

module.exports = nextConfig
